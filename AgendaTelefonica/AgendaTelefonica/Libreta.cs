﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgendaTelefonica
{
    public class Libreta
    {
        private List<Contacto> contactos;

        public Libreta()
        {
            contactos = new List<Contacto>();
        }

        public List<Contacto> Contactos
        {
            get
            {
                return contactos;
            }

            set
            {
                contactos = value;
            }
        }

        private bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public void LoadLib(string path)
        {
            string[] lineas = System.IO.File.ReadAllLines(path);
            if (lineas.Count() != 0 && lineas.ElementAt(0).Contains('/'))
            {
                foreach (string line in lineas)
                {
                    string[] bl = line.Split('/');
                    int nAtt = bl.Count();

                    //Name+LastName
                    if (nAtt == 2)
                        Contactos.Add(new Contacto(bl[0], bl[1]));
                    //Name+LastName+(Email||Num)
                    else if (nAtt == 3)
                    {
                        if (!IsDigitsOnly(bl[2]))
                            Contactos.Add(new Contacto(bl[0], bl[1], bl[2]));
                        else
                            Contactos.Add(new Contacto(bl[0], bl[1], Convert.ToInt32(bl[2])));
                    }
                    //Name+LastName+( (Nums)||(Email+Num) )
                    else if (nAtt >= 4)
                    {
                        if (IsDigitsOnly(bl[2]))
                        {
                            int tlfs = nAtt - 2;
                            int[] numbers = new int[tlfs];
                            for (int i = 0; i < tlfs; i++)
                            {
                                numbers[i] = Convert.ToInt32(bl[2 + i]);
                            }
                            Contactos.Add(new Contacto(bl[0], bl[1], numbers));

                        }
                        else if (!IsDigitsOnly(bl[2]) && IsDigitsOnly(bl[3]))
                        {
                            int tlfs = nAtt - 3;
                            int[] numbers = new int[tlfs];
                            for (int i = 0; i < tlfs; i++)
                            {
                                numbers[i] = Convert.ToInt32(bl[3 + i]);
                            }
                            Contactos.Add(new Contacto(bl[0], bl[1], bl[2], numbers));
                        }

                    }
                }
                Console.WriteLine("Libreta Cargada correctamente");
            }

        }
        
        public void SaveLib(string fileName)
        {
            using (System.IO.StreamWriter tf = new System.IO.StreamWriter(fileName)) {
                foreach (Contacto con in Contactos)
                {
                    /*
                    String line = null;
                    String nums = null;

                    line = String.Concat(con.Nombre, "/", con.Apellido);

                    if (con.Telefonos.Count() != 0)
                    {
                        foreach (int n in con.Telefonos)
                        {
                            nums = String.Concat(nums, "/", n);
                        }
                        line = String.Concat(line, "/", nums);
                    }
                    if (con.Email != null)
                        line = String.Concat(line, "/", con.Email);
                    */

                    tf.WriteLine(con.ToString());
                }
            }
            Console.WriteLine("Libreta Salvada correctamente como : " + fileName);

        }

        public void EraseLib()
        {
            contactos = new List<Contacto>();
            Console.WriteLine("Libreta vacía");
        }

        public Contacto SearchName(String n, String ln)
        {
            Contacto ret=null;

            foreach(Contacto con in Contactos)
            {
                if(con.Nombre == n && con.Apellido == ln)
                    ret = con;
            }

            if (ret == null)
                Console.WriteLine("No se encontro contacto con ese Nombre y Apellido");

            return ret;
        }

        public Contacto SearchNum(int num)
        {
            Contacto ret = null;

            foreach (Contacto con in Contactos)
            {
                if (con.Telefonos.Contains(num))
                    ret = con;
            }

            if (ret == null)
                Console.WriteLine("No se encontro contacto con ese Numero");

            return ret;
        }

        public void AddContact(String n, String ln)
        {
            Contactos.Add(new Contacto(n, ln));
        }

        public void AddContact(String n, String ln,String em)
        {
            Contactos.Add(new Contacto(n, ln,em));
        }

        public void AddContact(String n, String ln,String em, int num)
        {
            Contactos.Add(new Contacto(n, ln, em, num));
        }

        public void AddContact(String n, String ln,int num)
        {
            Contactos.Add(new Contacto(n, ln, num));    
        }

        public void EraseContact(String n, String ln)
        {
            foreach (Contacto con in Contactos)
            {
                if (con.Nombre == n && con.Apellido == ln)
                {
                    Contactos.Remove(con);
                    Console.WriteLine("CONTACTO eliminado");
                }
                else
                    Console.WriteLine("No existe Contacto con ese NOMBRE y APELLIDO");
            }
        }
        public void EraseContact(int n)
        {
            foreach (Contacto con in Contactos)
            {
                if (con.Telefonos.Contains(n))
                {
                    Contactos.Remove(con);
                    Console.WriteLine("CONTACTO eliminado");
                }
                else
                    Console.WriteLine("No existe Contacto con ese NUMERO DE TELEFONO");
            }
        }

        public void EraseNum(int n)
        {
            foreach (Contacto con in Contactos)
            {
                if (con.Telefonos.Contains(n))
                {
                    con.EraseNumber(n);
                    Console.WriteLine("NUMERO eliminado");
                }
            }
        }

        public void ViewLib()
        {
            Console.WriteLine("------------------------");
            Console.WriteLine("Tu Agenda contiene " + Contactos.Count().ToString() + " contactos :");
            Console.WriteLine("----------");
            int i=1;
            foreach(Contacto con in Contactos)
            {
                Console.WriteLine("Contacto # " + i.ToString());
                con.DisplayInfo();
                Console.WriteLine("----------");
            }
            Console.WriteLine("------------------------");
        }

        public void AddNumContact(String n, String ln, int num)
        {
            SearchName(n, ln).AddNumber(num);   
        }
    }
}