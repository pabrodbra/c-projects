﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//FALTA TRABAJAR CON LAS EXCEPCIONES - FICHEROS

namespace AgendaTelefonica
{
    class Program
    {
        static void Main(string[] args)
        {
            Libreta libreta = new Libreta();
            String name = null, lastname = null, email = null, number = null;

            Console.WriteLine("WELCOME - BIENVENIDO - WILKOMME - BENVENUTO");
            Console.WriteLine("--------LIBRETA ELECTRONICA v.0.1--------");
            Console.WriteLine("Made by: BaseKing - 2016");
            Console.WriteLine("");
            Console.WriteLine("");

            Console.WriteLine("Para ayuda de como utilizar escriba /help");
            Console.WriteLine("");
            string s;
            while ((s=Console.ReadLine()) != "/exit")
            {
                switch (s)
                {
                    case "/help":
                        Console.WriteLine("-------------------");
                        Console.WriteLine("COMANDOS DE LIBRETA");
                        Console.WriteLine("/viewlib - Ver todas las entradas en la libreta");
                        Console.WriteLine("/loadlib - Cargar una libreta electronica de un archivo .txt");
                        Console.WriteLine("/savelib - Salvar la libreta actual a un archivo .txt");
                        Console.WriteLine("/eraselib - Borrar toda la libreta actual");
                        Console.WriteLine("");
                        Console.WriteLine("-------------------");
                        Console.WriteLine("COMANDOS DE CONTACTOS");
                        Console.WriteLine("/addc - Añadir contacto");
                        Console.WriteLine("/addcn - Añadir un numero a un contacto");
                        Console.WriteLine("/erasec - Borrar un contacto por nombre y apellido");
                        Console.WriteLine("/erasecnum - Borrar un contacto por numero de telefono");
                        Console.WriteLine("/erasenum - Borrar un numero de la libreta");
                        Console.WriteLine("/searchname - Buscar la informacion de un contacto conociendo el nombre y apellido");
                        Console.WriteLine("/searchnum - Buscar la informacion de un contacto conociendo el numero de telefono");
                        Console.WriteLine("");
                        Console.WriteLine("-------------------");
                        break;
                    case "/loadlib":
                        Console.WriteLine("Por favor inserte el nombre/direccion del archivo :");
                        libreta.LoadLib(Console.ReadLine() + ".txt");
                        break;
                    case "/savelib":
                        Console.WriteLine("Por favor inserte el nombre que desea poner a esta version de la libreta :");
                        libreta.SaveLib(Console.ReadLine() + ".txt");
                        break;
                    case "/eraselib":
                        libreta.EraseLib();
                        break;
                    case "/viewlib":
                        libreta.ViewLib();
                        break;
                    case "/addc":
                        Console.WriteLine("Introduzca los siguientes datos. Si no lo posee escriba -np :");
                        Console.WriteLine("NOMBRE :");
                        name = Console.ReadLine();
                        Console.WriteLine("APELLIDO :");
                        lastname = Console.ReadLine();
                        Console.WriteLine("EMAIL :");
                        email = Console.ReadLine();
                        Console.WriteLine("NUMERO DE TELEFONO :");
                        number = Console.ReadLine();
                        if (email == "-np")
                        {
                            libreta.AddContact(name, lastname, Convert.ToInt32(number));
                        }
                        else if (number == "-np")
                        {
                            libreta.AddContact(name, lastname, email);
                        }
                        else if (email == "-np" && number == "-np")
                        {
                            libreta.AddContact(name, lastname);
                        }
                        else
                            libreta.AddContact(name, lastname, email, Convert.ToInt32(number));
                        break;
                    case "/addcn":
                        Console.WriteLine("Escriba el NOMBRE y APELLIDO del numero que desea agregar :");
                        Console.WriteLine("NOMBRE :");
                        name = Console.ReadLine();
                        Console.WriteLine("APELLIDO :");
                        lastname = Console.ReadLine();
                        Console.WriteLine("Escriba el numero de telefono que desea añadir :");
                        number = Console.ReadLine();
                        libreta.AddNumContact(name, lastname, Convert.ToInt32(number));
                        break;
                    case "/erasec":
                        Console.WriteLine("Escriba el NOMBRE y APELLIDO de la persona que desea borrar");
                        Console.WriteLine("NOMBRE :");
                        name = Console.ReadLine();
                        Console.WriteLine("APELLIDO :");
                        lastname = Console.ReadLine();
                        libreta.EraseContact(name, lastname);
                        break;
                    case "/erasecnum":
                        Console.WriteLine("Escriba el NUMERO DE TELEFONO de la PERSONA que desea eliminar :");
                        number = Console.ReadLine();
                        libreta.EraseNum(Convert.ToInt32(number));
                        break;
                    case "/erasenum":
                        Console.WriteLine("Escriba el NUMERO DE TELEFONO que desea eliminar :");
                        number = Console.ReadLine();
                        libreta.EraseNum(Convert.ToInt32(number));
                        break;
                    case "/searchname":
                        Console.WriteLine("Escriba el NOMBRE y APELLIDO que desea buscar :");
                        Console.WriteLine("NOMBRE :");
                        name = Console.ReadLine();
                        Console.WriteLine("APELLIDO :");
                        lastname = Console.ReadLine();
                        libreta.SearchName(name, lastname).DisplayInfo();
                        break;
                    case "/searchnum":
                        Console.WriteLine("Escriba el NUMERO DE TELEFONO que desea buscar");
                        number = Console.ReadLine();
                        libreta.SearchNum(Convert.ToInt32(number)).DisplayInfo();
                        break;
                    default:
                        Console.WriteLine("No es un comando valido, por favor pruebe otro o escriba /help");
                        break;
                }
            }

        }
    }
}

//Console.WriteLine("");
/*
 *
Comandos de Libreta:
/viewlib - Ver todas las entradas en la libreta
/loadlib - Cargar una libreta electronica de un archivo .txt
/savelib - Salvar la libreta actual a un archivo .txt
/eraselib - Borrar toda la libreta actual

Comandos de Contactos
/addc - Añadir contacto
/addcn - Añadir un numero a un contacto
/erasec - Borrar un contacto por nombre y apellido
/erasecnum - Borrar un contacto por numero de telefono
/erasenum - Borrar un numero de la libreta
/searchname - Buscar la informacion de un contacto conociendo el nombre y apellido
/searchnum - Buscar la informacion de un contacto conociendo el numero de telefono

 /exit - Para salir
 * 
 */
