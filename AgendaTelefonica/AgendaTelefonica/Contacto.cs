﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgendaTelefonica
{
    public class Contacto
    {

        private String nombre;
        private String apellido;
        private List<int> telefonos;
        private String email;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public List<int> Telefonos
        {
            get
            {
                return telefonos;
            }

            set
            {
                telefonos = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public void AddNumber(int n)
        {
            Telefonos.Add(n);
        }

        public void DisplayInfo()
        {
            Console.WriteLine("Nombre : " + Nombre);
            Console.WriteLine("Apellido : " + Apellido);
            int c = 1;
            if (Telefonos.Count() != 0)
                foreach(int num in Telefonos)
                {
                    Console.WriteLine("Telefono #" + c.ToString() + " : " + num.ToString());
                    c++;
                }
            else
                Console.WriteLine("No posee # de telefono asociado");

            if(Email!=null)
                Console.WriteLine("Email : " + Email);
        }

        public void EraseNumber(int n)
        {
            if (Telefonos.Contains(n))
            {
                Telefonos.Remove(n);
            }
        }

        public override String ToString()
        {
            String line = null;
            String nums = null;

            line = String.Concat(Nombre, "/", Apellido);

            if (Email != null)
                line = String.Concat(line, "/", Email);

            if (Telefonos.Count() != 0)
            {
                foreach (int n in Telefonos)
                {
                    if (nums != null)
                        nums = String.Concat(nums, "/", n.ToString());
                    else if (nums == null)
                        nums = n.ToString();
                }
                line = String.Concat(line, "/", nums);
            }

            return line;
        }

        public Contacto(String n, String ln, String ema, params int[] num)
        {
            nombre = n;
            apellido = ln;
            email = ema;
            telefonos = new List<int>();
            foreach (int v in num)
                telefonos.Add(v);
        }
        public Contacto(String n, String ln, params int[] num)
        {
            nombre = n;
            apellido = ln;
            email = null;
            telefonos = new List<int>();
            foreach(int v in num)
                telefonos.Add(v);
        }
        public Contacto(String n, String ln, String ema)
        {
            nombre = n;
            apellido = ln;
            email = ema;
            telefonos = new List<int>();
        }
        public Contacto(String n, String ln)
        {
            nombre = n;
            apellido = ln;
            email = null;
            telefonos = new List<int>();
        }
    }
}