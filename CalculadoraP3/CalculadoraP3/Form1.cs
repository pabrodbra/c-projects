﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraP3
{
    public partial class Form1 : Form
    {
        int num1 = 0, num2 = 0, res = 0;
        char operador = '0';
        public Form1()
        {
            InitializeComponent();
        }

        public void buttonPress(int x)
        {
            if (tResult.Text.Equals("0") && x == 0)
            {
                tResult.Text = "0";
            }
            else
            {
                if (tResult.Text.Equals("0"))
                    tResult.Text = "";
                tResult.Text = tResult.Text + x;
            }
        }

        private void b1_Click(object sender, EventArgs e)
        {
            int but = 1;
            buttonPress(but);
        }

        private void b2_Click(object sender, EventArgs e)
        {
            int but = 2;
            buttonPress(but);
        }

        private void b3_Click(object sender, EventArgs e)
        {
            int but = 3;
            buttonPress(but);
        }

        private void b4_Click(object sender, EventArgs e)
        {
            int but = 4;
            buttonPress(but);
        }

        private void b5_Click(object sender, EventArgs e)
        {
            int but = 5;
            buttonPress(but);
        }

        private void b6_Click(object sender, EventArgs e)
        {
            int but = 6;
            buttonPress(but);
        }

        private void b7_Click(object sender, EventArgs e)
        {
            int but = 7;
            buttonPress(but);
        }

        private void b8_Click(object sender, EventArgs e)
        {
            int but = 8;
            buttonPress(but);
        }

        private void b9_Click(object sender, EventArgs e)
        {
            int but = 9;
            buttonPress(but);
        }

        private void b0_Click(object sender, EventArgs e)
        {
            int but = 0;
            buttonPress(but);
        }

        private void blimpiar_Click(object sender, EventArgs e)
        {
            tResult.Text = "0";
        }

        private void bborrar_Click(object sender, EventArgs e)
        {
            String resact = tResult.Text;

            if (!resact.Equals("0") || !resact.Equals(""))
            {
                tResult.Text = tResult.Text.Substring(0, resact.Count() - 1);
                if (tResult.Text.ElementAt(0).Equals('-') && tResult.Text.Count() == 1)
                    tResult.Text = "0";
            }

        }

        private void bsigno_Click(object sender, EventArgs e)
        {
            if (!tResult.Text.Contains('-'))
            {
                if (!tResult.Text.Equals("0"))
                    tResult.Text = '-' + tResult.Text;
                else
                    tResult.Text = "-";
            }
            else if (tResult.Text.Contains('-'))
            {
                tResult.Text = tResult.Text.Substring(1);
            }
        }

        private void bmas_Click(object sender, EventArgs e)
        {
            if (tResult.Text.Contains(','))
            {
                tResult.Text = tResult.Text.Substring(0, tResult.Text.IndexOf(','));
            }

            try
            {
                num1 = Convert.ToInt32(tResult.Text);
                operador = '+';
            }
            catch
            {
                MessageBox.Show(tResult.Text + " es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //num1 = Convert.ToInt32(tResult.Text);
            tResult.Text = "0";
        }

        private void bmenos_Click(object sender, EventArgs e)
        {
            if (tResult.Text.Contains(','))
            {
                tResult.Text.Substring(0, tResult.Text.IndexOf(','));
            }
            //num1 = Convert.ToInt32(tResult.Text);
            try
            {
                num1 = Convert.ToInt32(tResult.Text);
                operador = '-';
            }
            catch
            {
                MessageBox.Show(tResult.Text + " es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            tResult.Text = "0";
        }

        private void bpor_Click(object sender, EventArgs e)
        {
            if (tResult.Text.Contains(','))
            {
                tResult.Text.Substring(0, tResult.Text.IndexOf(','));
            }
            //num1 = Convert.ToInt32(tResult.Text);
            try
            {
                num1 = Convert.ToInt32(tResult.Text);
                operador = '*';
            }
            catch
            {
                MessageBox.Show(tResult.Text + " es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            tResult.Text = "0";
        }

        private void bentre_Click(object sender, EventArgs e)
        {
            if (tResult.Text.Contains(','))
            {
                tResult.Text.Substring(0, tResult.Text.IndexOf(','));
            }
            //num1 = Convert.ToInt32(tResult.Text);
            try
            {
                num1 = Convert.ToInt32(tResult.Text);
                operador = '/';
            }
            catch
            {
                MessageBox.Show(tResult.Text + "es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            tResult.Text = "0";
        }

        private void bequal_Click(object sender, EventArgs e)
        {
            //num2 = Convert.ToInt32(tResult.Text);
            try
            {
                num2 = Convert.ToInt32(tResult.Text);
                switch (operador)
                {
                    case '+':
                        //res = num1 + num2;
                        //tResult.Text = res.ToString();
                        try
                        {
                            res = num1 + num2;
                            tResult.Text = res.ToString();
                        }
                        catch
                        {
                            MessageBox.Show("El resultado es un valor no aceptable en INT32, pruebe con otros sumandos", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case '-':
                        //res = num1 - num2;
                        //tResult.Text = res.ToString();
                        try
                        {
                            res = num1 - num2;
                            tResult.Text = res.ToString();
                        }
                        catch
                        {
                            MessageBox.Show("El resultado es un valor no aceptable en INT32, pruebe con otro minuendo y/o sustraendo", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case '*':
                        //res = num1 * num2;
                        //tResult.Text = res.ToString();
                        try
                        {
                            res = num1 * num2;
                            tResult.Text = res.ToString();
                        }
                        catch
                        {
                            MessageBox.Show("El resultado es un valor no aceptable en INT32, pruebe con otros factores", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case '/':
                        /*float resdiv=0;
                        if (num2 != 0)
                            resdiv = (float)num1 / (float)num2;
                        tResult.Text = resdiv.ToString();*/
                        try
                        {
                            float resdiv = 0;
                            if (num2 != 0)
                                resdiv = (float)num1 / (float)num2;
                            tResult.Text = resdiv.ToString();
                        }
                        catch
                        {
                            MessageBox.Show("El resultado es un valor no aceptable en INT32, pruebe con otro divisor y/o dividendo", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case '0':
                        break;
                }
            }
            catch
            {
                MessageBox.Show(tResult.Text + "es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
    }
}

        /*
        try{
            
        }
        catch{
            MessageBox.Show(tResult.Text + "es un valor no aceptable en INT32, pruebe con otro valor", "Stack Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        */
