﻿namespace CalculadoraP3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.blimpiar = new System.Windows.Forms.Button();
            this.bborrar = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.bsigno = new System.Windows.Forms.Button();
            this.b0 = new System.Windows.Forms.Button();
            this.bequal = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.bmas = new System.Windows.Forms.Button();
            this.tResult = new System.Windows.Forms.TextBox();
            this.bmenos = new System.Windows.Forms.Button();
            this.bpor = new System.Windows.Forms.Button();
            this.bentre = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // blimpiar
            // 
            this.blimpiar.Location = new System.Drawing.Point(13, 64);
            this.blimpiar.Name = "blimpiar";
            this.blimpiar.Size = new System.Drawing.Size(119, 28);
            this.blimpiar.TabIndex = 0;
            this.blimpiar.Text = "Limpiar";
            this.blimpiar.UseVisualStyleBackColor = true;
            this.blimpiar.Click += new System.EventHandler(this.blimpiar_Click);
            // 
            // bborrar
            // 
            this.bborrar.Location = new System.Drawing.Point(166, 64);
            this.bborrar.Name = "bborrar";
            this.bborrar.Size = new System.Drawing.Size(118, 28);
            this.bborrar.TabIndex = 1;
            this.bborrar.Text = "Borrar";
            this.bborrar.UseVisualStyleBackColor = true;
            this.bborrar.Click += new System.EventHandler(this.bborrar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.Controls.Add(this.bsigno, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.b0, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.bequal, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.b7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.b8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.b9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.b4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.b5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.b3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.b1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.b2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.b6, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 98);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.91304F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.08696F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(184, 180);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // bsigno
            // 
            this.bsigno.Location = new System.Drawing.Point(3, 136);
            this.bsigno.Name = "bsigno";
            this.bsigno.Size = new System.Drawing.Size(55, 39);
            this.bsigno.TabIndex = 11;
            this.bsigno.Text = "+/-";
            this.bsigno.UseVisualStyleBackColor = true;
            this.bsigno.Click += new System.EventHandler(this.bsigno_Click);
            // 
            // b0
            // 
            this.b0.Location = new System.Drawing.Point(64, 136);
            this.b0.Name = "b0";
            this.b0.Size = new System.Drawing.Size(55, 39);
            this.b0.TabIndex = 9;
            this.b0.Text = "0";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.b0_Click);
            // 
            // bequal
            // 
            this.bequal.Location = new System.Drawing.Point(125, 136);
            this.bequal.Name = "bequal";
            this.bequal.Size = new System.Drawing.Size(55, 39);
            this.bequal.TabIndex = 10;
            this.bequal.Text = "=";
            this.bequal.UseVisualStyleBackColor = true;
            this.bequal.Click += new System.EventHandler(this.bequal_Click);
            // 
            // b7
            // 
            this.b7.Location = new System.Drawing.Point(3, 92);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(55, 38);
            this.b7.TabIndex = 8;
            this.b7.Text = "7";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b8
            // 
            this.b8.Location = new System.Drawing.Point(64, 92);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(55, 38);
            this.b8.TabIndex = 6;
            this.b8.Text = "8";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // b9
            // 
            this.b9.Location = new System.Drawing.Point(125, 92);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(55, 38);
            this.b9.TabIndex = 7;
            this.b9.Text = "9";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b4
            // 
            this.b4.Location = new System.Drawing.Point(3, 47);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(55, 39);
            this.b4.TabIndex = 5;
            this.b4.Text = "4";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // b5
            // 
            this.b5.Location = new System.Drawing.Point(64, 47);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(55, 39);
            this.b5.TabIndex = 3;
            this.b5.Text = "5";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b3
            // 
            this.b3.Location = new System.Drawing.Point(125, 3);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(55, 38);
            this.b3.TabIndex = 2;
            this.b3.Text = "3";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // b1
            // 
            this.b1.Location = new System.Drawing.Point(3, 3);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(55, 38);
            this.b1.TabIndex = 0;
            this.b1.Text = "1";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b2
            // 
            this.b2.Location = new System.Drawing.Point(64, 3);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(55, 38);
            this.b2.TabIndex = 1;
            this.b2.Text = "2";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b6
            // 
            this.b6.Location = new System.Drawing.Point(125, 47);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(55, 39);
            this.b6.TabIndex = 4;
            this.b6.Text = "6";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // bmas
            // 
            this.bmas.Location = new System.Drawing.Point(216, 100);
            this.bmas.Name = "bmas";
            this.bmas.Size = new System.Drawing.Size(68, 41);
            this.bmas.TabIndex = 3;
            this.bmas.Text = "+";
            this.bmas.UseVisualStyleBackColor = true;
            this.bmas.Click += new System.EventHandler(this.bmas_Click);
            // 
            // tResult
            // 
            this.tResult.Enabled = false;
            this.tResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tResult.Location = new System.Drawing.Point(13, 16);
            this.tResult.Name = "tResult";
            this.tResult.Size = new System.Drawing.Size(271, 26);
            this.tResult.TabIndex = 4;
            this.tResult.Text = "0";
            this.tResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // bmenos
            // 
            this.bmenos.Location = new System.Drawing.Point(216, 144);
            this.bmenos.Name = "bmenos";
            this.bmenos.Size = new System.Drawing.Size(68, 41);
            this.bmenos.TabIndex = 5;
            this.bmenos.Text = "-";
            this.bmenos.UseVisualStyleBackColor = true;
            this.bmenos.Click += new System.EventHandler(this.bmenos_Click);
            // 
            // bpor
            // 
            this.bpor.Location = new System.Drawing.Point(216, 189);
            this.bpor.Name = "bpor";
            this.bpor.Size = new System.Drawing.Size(68, 41);
            this.bpor.TabIndex = 6;
            this.bpor.Text = "*";
            this.bpor.UseVisualStyleBackColor = true;
            this.bpor.Click += new System.EventHandler(this.bpor_Click);
            // 
            // bentre
            // 
            this.bentre.Location = new System.Drawing.Point(216, 234);
            this.bentre.Name = "bentre";
            this.bentre.Size = new System.Drawing.Size(68, 41);
            this.bentre.TabIndex = 7;
            this.bentre.Text = "/";
            this.bentre.UseVisualStyleBackColor = true;
            this.bentre.Click += new System.EventHandler(this.bentre_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Hecho por: Pablo Rodriguez";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 297);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bentre);
            this.Controls.Add(this.bpor);
            this.Controls.Add(this.bmenos);
            this.Controls.Add(this.tResult);
            this.Controls.Add(this.bmas);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.bborrar);
            this.Controls.Add(this.blimpiar);
            this.Name = "Form1";
            this.Text = "Calculadora v0.1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button blimpiar;
        private System.Windows.Forms.Button bborrar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button bsigno;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.Button bequal;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button bmas;
        private System.Windows.Forms.TextBox tResult;
        private System.Windows.Forms.Button bmenos;
        private System.Windows.Forms.Button bpor;
        private System.Windows.Forms.Button bentre;
        private System.Windows.Forms.Label label1;
    }
}

